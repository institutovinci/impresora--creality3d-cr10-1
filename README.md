# impresora--creality3d-cr10-1

Información y configuración de la impresora Creality 3D CR-10 [1]

## Estado

	- Firmware original.
	
## Perfiles

### Ultimaker Cura

_Esto está desactualizado. Lo cambiamos por https://gitlab.com/institutovinci/perfiles-cura y https://gitlab.com/institutovinci/scripts-actualizacion--automatica-cura ._

[Cómo cargar impresoras y perfiles en Ultimaker Cura](https://www.youtube.com/watch?v=sRDgST4yU9A)

- `vinci-cr10.curaproject.3mf`: definición de la máquina.
- `vinci-cr10-02mm-50mms-v1.curaprofile`: perfil de base. 0,2 mm de altura de capa y 50 mm/s de velocidad base.

## Máquina

### Características

- A completar

### Modificaciones

- A completar